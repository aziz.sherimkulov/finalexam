﻿using System.Collections.Generic;
using System.Threading.Tasks;
using FinalExam.DAL.Entity;

namespace FinalExam.DAL.Repository
{
    public interface IRepository<T> where T : class, IEntity
    {
        Task<int> CreateAsync(T entity);
        Task<List<T>> GetAllAsync();
        Task<T> GetByIdAsync(int id);
        Task UpdateAsync(T entity);
        Task RemoveAsync(T entity);
    }
}