﻿namespace FinalExam.DAL.Entity
{
    public interface IEntity
    {
        int Id { get; set; }
    }
}