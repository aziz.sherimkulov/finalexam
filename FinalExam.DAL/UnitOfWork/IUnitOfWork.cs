﻿using System;
using System.Threading.Tasks;

namespace FinalExam.DAL.UnitOfWork
{
    public interface IUnitOfWork : IDisposable
    {
        Task<int> CompleteAsync();
    }
}