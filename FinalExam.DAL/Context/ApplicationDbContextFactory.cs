﻿using Microsoft.EntityFrameworkCore;

namespace FinalExam.DAL
{
    public class ApplicationDbContextFactory : IApplicationDbContextFactory
    {
        private readonly DbContextOptions _options;

        public ApplicationDbContextFactory(DbContextOptions options)
        {
            _options = options;
        }

        public ApplicationDbContext CreateDbContext()
        {
            return new ApplicationDbContext(_options);
        }
    }

    public interface IApplicationDbContextFactory
    {
        ApplicationDbContext CreateDbContext();
    }
}